package service

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
)

type store interface {
	CreateWallet(ctx context.Context, wallet model.Wallet) (*model.Wallet, error)
	GetWallets(ctx context.Context) ([]*model.Wallet, error)
	GetWalletByID(ctx context.Context, walletID uuid.UUID) (*model.Wallet, error)
	DeleteWallet(ctx context.Context, walletID uuid.UUID) error
	UpdateWallet(ctx context.Context, walletID uuid.UUID, request model.UpdateWalletRequest) (*model.Wallet, error)
	ExternalTransaction(ctx context.Context, transaction model.Transaction) (*uuid.UUID, error)
}

type currencyConverter interface {
	GetExchangeRate(baseCurrency, targetCurrency string) (float64, error)
}

type Service struct {
	db store
	cc currencyConverter
}

func New(db store, cc currencyConverter) *Service {
	return &Service{
		db: db,
		cc: cc,
	}
}

func (s *Service) CreateWallet(ctx context.Context, wallet model.Wallet) (*model.Wallet, error) {
	rWallet, err := s.db.CreateWallet(ctx, wallet)
	if err != nil {
		return nil, fmt.Errorf("s.db.CreateWallet(ctx, owner, currency): %w", err)
	}

	return rWallet, nil
}

func (s *Service) GetWalletByID(ctx context.Context, walletID uuid.UUID) (*model.Wallet, error) {
	wallet, err := s.db.GetWalletByID(ctx, walletID)
	if err != nil {
		return nil, fmt.Errorf("s.db.GetWalletByID(ctx, WalletID): %w", err)
	}

	return wallet, nil
}

func (s *Service) GetWallets(ctx context.Context) ([]*model.Wallet, error) {
	wallets, err := s.db.GetWallets(ctx)
	if err != nil {
		return nil, fmt.Errorf("s.db.GetWallets(ctx, owner): %w", err)
	}

	return wallets, nil
}

func (s *Service) DeleteWallet(ctx context.Context, walletID uuid.UUID) error {
	err := s.db.DeleteWallet(ctx, walletID)
	if err != nil {
		return fmt.Errorf("s.db.DeleteWallet(ctx, walletID): %w", err)
	}

	return nil
}

func (s *Service) UpdateWallet(
	ctx context.Context,
	walletID uuid.UUID,
	request model.UpdateWalletRequest,
) (*model.Wallet, error) {
	wallet, err := s.db.GetWalletByID(ctx, walletID)
	if err != nil {
		return nil, fmt.Errorf("s.db.GetWalletByID(ctx, walletID): %w", err)
	}

	if request.Currency != nil && *request.Currency != wallet.Currency {
		xr, err := s.cc.GetExchangeRate(wallet.Currency, *request.Currency)
		if err != nil {
			return nil, fmt.Errorf("s.cc.GetExchangeRate(*request.Currency, wallet.Currency): %w", err)
		}

		request.ConversionRate = xr
	} else {
		request.ConversionRate = 1
	}

	wallet, err = s.db.UpdateWallet(ctx, walletID, request)
	if err != nil {
		return nil, fmt.Errorf("s.db.UpdateWallet(ctx, walletID, request): %w", err)
	}

	return wallet, nil
}

func (s *Service) ExternalTransaction(ctx context.Context, transaction model.Transaction) (*uuid.UUID, error) {
	// conversion
	wallet, err := s.db.GetWalletByID(ctx, *transaction.TargetWalletID)
	if err != nil {
		return nil, fmt.Errorf("s.db.GetWalletByID(ctx, *transaction.TargetWalletID): %w", err)
	}

	if wallet.Currency != transaction.Currency {
		k, err := s.cc.GetExchangeRate(transaction.Currency, wallet.Currency)
		if err != nil {
			return nil, fmt.Errorf("s.cc.GetExchangeRate(transaction.Currency, wallet.Currency): %w", err)
		}

		transaction.Currency = wallet.Currency
		transaction.Sum *= k
	}

	// execution
	transactionID, err := s.db.ExternalTransaction(ctx, transaction)
	if err != nil {
		return nil, fmt.Errorf("s.db.ExternalTransaction(ctx, transaction): %w", err)
	}

	return transactionID, nil
}
