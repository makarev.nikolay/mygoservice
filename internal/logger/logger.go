package logger

import (
	"os"

	"gitlab.com/makarev.nikolay/mygoservice/internal/config"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func InitLogger(cfg *config.Config) error {
	level, err := zap.ParseAtomicLevel(cfg.LogConf.Level)
	if err != nil {
		return err
	}

	encoderConfig := zap.NewProductionEncoderConfig()

	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	zap.ReplaceGlobals(zap.New(zapcore.NewCore(zapcore.NewConsoleEncoder(encoderConfig), os.Stdout, level)))

	zap.L().Info("successful logger initialization")
	return nil
}
