package model

import (
	"errors"
)

var (
	ErrWalletNotFound       = errors.New("wallet not found")
	ErrUserNotFound         = errors.New("user not found")
	ErrNilUUID              = errors.New("uuid is nil")
	ErrNotAllowed           = errors.New("not allowed")
	ErrDuplicateWallet      = errors.New("duplicate wallet")
	ErrWrongCurrency        = errors.New("wrong currency")
	ErrGettingXR            = errors.New("error getting xr")
	ErrNegativeSum          = errors.New("negative sum in request")
	ErrZeroSum              = errors.New("sum can't be zero")
	ErrNotEnoughBalance     = errors.New("not enough balance")
	ErrWalletWasChanged     = errors.New("wallet was changed in parallel")
	ErrDuplicateTransaction = errors.New("transactions id already exists")
	ErrInvalidAccessToken   = errors.New("err invalid access token")
)
