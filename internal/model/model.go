package model

import (
	"github.com/golang-jwt/jwt/v5"
	"time"

	"github.com/google/uuid"
)

type ctxKey string

const (
	UserInfoKey ctxKey = "userInfo"
)

type Transaction struct {
	ID             uuid.UUID  `json:"id"`
	CreatedAt      time.Time  `json:"createdAt"`
	AgentWalletID  *uuid.UUID `json:"agentWalletId,omitempty"`
	TargetWalletID *uuid.UUID `json:"targetWalletId,omitempty"`
	Currency       string     `json:"currency"`
	Sum            float64    `json:"sum"`
}

func (t *Transaction) Validate() error {
	switch {
	case t.Sum == 0:
		return ErrZeroSum
	case t.Sum < 0:
		return ErrNegativeSum
	case t.TargetWalletID == nil:
		return ErrWalletNotFound
	case t.ID == uuid.Nil:
		return ErrNilUUID
	}

	return nil
}

type Claims struct {
	jwt.RegisteredClaims
	UUID uuid.UUID `json:"uuid"`
}

type Wallet struct {
	ID           uuid.UUID `json:"id"`
	OwnerID      uuid.UUID `json:"ownerId"`
	Currency     string    `json:"currency"`
	Balance      float64   `json:"balance"`
	CreatedDate  time.Time `json:"createdDate"`
	ModifiedDate time.Time `json:"modifiedDate"`
	Name         string    `json:"name"`
}

type UpdateWalletRequest struct {
	Name           *string `json:"name,omitempty"`
	Currency       *string `json:"currency,omitempty"`
	ConversionRate float64 `json:"conversionRate,omitempty"`
}

type User struct {
	ID      uuid.UUID `json:"id"`
	Email   string    `json:"email"`
	RegDate time.Time `json:"regDate"`
}

type XRRequest struct {
	BaseCurrency   string `schema:"base"`
	TargetCurrency string `schema:"target"`
}

type XRResponse struct {
	XR float64 `json:"xr"`
}

type UserInfo struct {
	ID uuid.UUID
}
