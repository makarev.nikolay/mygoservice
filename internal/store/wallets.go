package store

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5/pgconn"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v5"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
	"go.uber.org/zap"
)

func (p *Postgres) TruncateTables(ctx context.Context) error {
	_, err := p.Db.Exec(
		ctx,
		"TRUNCATE TABLE transactions CASCADE")
	if err != nil {
		return fmt.Errorf("p.db.Exec(...): %w", err)
	}

	_, err = p.Db.Exec(
		ctx,
		"TRUNCATE TABLE wallets CASCADE")
	if err != nil {
		return fmt.Errorf("p.db.Exec(...): %w", err)
	}

	_, err = p.Db.Exec(
		ctx,
		"TRUNCATE TABLE users CASCADE")
	if err != nil {
		return fmt.Errorf("p.db.Exec(...): %w", err)
	}

	return nil
}

func (p *Postgres) CreateWallet(ctx context.Context, wallet model.Wallet) (*model.Wallet, error) {
	if wallet.OwnerID == uuid.Nil {
		return nil, model.ErrNilUUID
	}

	// Checking if name is free
	query := `
	SELECT FROM wallets
	WHERE is_disabled = false and owner_id = $1 and name = $2`
	err := p.Db.QueryRow(
		ctx,
		query,
		wallet.OwnerID, wallet.Name).Scan()

	switch {
	case errors.Is(err, pgx.ErrNoRows):
		break
	case err != nil:
		return nil, fmt.Errorf("p.db.QueryRow(...): %w", err)
	default:
		return nil, model.ErrDuplicateWallet
	}

	// Creating wallet
	query = `
    INSERT INTO wallets (id, owner_id, currency, name)
    VALUES ($1, $2, $3, $4)
    RETURNING id, owner_id, currency, balance, created_at, modified_at`

	err = p.Db.QueryRow(
		ctx,
		query,
		uuid.New(),
		wallet.OwnerID,
		wallet.Currency,
		wallet.Name,
	).Scan(
		&wallet.ID,
		&wallet.OwnerID,
		&wallet.Currency,
		&wallet.Balance,
		&wallet.CreatedDate,
		&wallet.ModifiedDate,
	)

	var pgErr *pgconn.PgError

	switch {
	case errors.As(err, &pgErr) && pgErr.Code == pgerrcode.ForeignKeyViolation:
		return nil, model.ErrUserNotFound
	case err != nil:
		return nil, fmt.Errorf("p.db.QueryRow(): %w", err)
	}

	return &wallet, nil
}

func (p *Postgres) GetWalletByID(ctx context.Context, walletID uuid.UUID) (*model.Wallet, error) {
	wallet := new(model.Wallet)
	query := `
	SELECT id, owner_id, currency, balance, created_at, modified_at, name
	FROM wallets 
	WHERE is_disabled = false and id = $1`

	err := p.Db.QueryRow(
		ctx,
		query,
		walletID,
	).Scan(
		&wallet.ID,
		&wallet.OwnerID,
		&wallet.Currency,
		&wallet.Balance,
		&wallet.CreatedDate,
		&wallet.ModifiedDate,
		&wallet.Name,
	)

	switch {
	case errors.Is(err, pgx.ErrNoRows):
		return nil, model.ErrWalletNotFound
	case err != nil:
		return nil, fmt.Errorf("p.db.QueryRow: %w", err)
	}

	return wallet, nil
}

func (p *Postgres) GetWallets(ctx context.Context) ([]*model.Wallet, error) {
	wallets := make([]*model.Wallet, 0, 1)

	query := `
	SELECT id, owner_id, currency, balance, created_at, modified_at, name
	FROM wallets 
	WHERE is_disabled = false
	`

	rows, err := p.Db.Query(
		ctx,
		query)
	if err != nil {
		return nil, fmt.Errorf("p.db.Query(ctx, query, owner.ID): %w", err)
	}
	defer rows.Close()

	for rows.Next() {
		wallet := new(model.Wallet)

		err = rows.Scan(
			&wallet.ID,
			&wallet.OwnerID,
			&wallet.Currency,
			&wallet.Balance,
			&wallet.CreatedDate,
			&wallet.ModifiedDate,
			&wallet.Name)
		if err != nil {
			return nil, fmt.Errorf("err = rows.Scan(...): %w", err)
		}

		wallets = append(wallets, wallet)
	}

	return wallets, nil
}

func (p *Postgres) DeleteWallet(ctx context.Context, walletID uuid.UUID) error {
	query := `
	UPDATE wallets
	SET is_disabled = true, modified_at = $2
	WHERE is_disabled = false and id = $1 
	RETURNING id
`
	err := p.Db.QueryRow(
		ctx,
		query,
		walletID, time.Now(),
	).Scan(nil)

	switch {
	case errors.Is(err, pgx.ErrNoRows):
		return model.ErrWalletNotFound
	case err != nil:
		return fmt.Errorf("p.db.QueryRow(...): %w", err)
	}

	return nil
}

func (p *Postgres) UpdateWallet(
	ctx context.Context,
	walletID uuid.UUID,
	request model.UpdateWalletRequest,
) (*model.Wallet, error) {
	wallet, err := p.GetWalletByID(ctx, walletID)

	if err != nil {
		return nil, fmt.Errorf("p.GetWalletByID(ctx, walletID): %w", err)
	}

	tx, err := p.Db.Begin(ctx)
	if err != nil {
		return nil, fmt.Errorf("p.db.Begin(ctx): %w", err)
	}

	defer func() {
		err := tx.Rollback(ctx)
		if err != nil && !errors.Is(err, pgx.ErrTxClosed) {
			zap.L().With(zap.Error(err)).Warn("UpdateWallet/tx.Rollback(ctx)")
		}
	}()

	if request.Name != nil {
		// checking if name is free
		query := `
		SELECT FROM wallets
		WHERE is_disabled = false and owner_id = $1 and name = $2`

		err := p.Db.QueryRow(
			ctx,
			query,
			wallet.OwnerID, request.Name).Scan()

		switch {
		case errors.Is(err, pgx.ErrNoRows):
			break
		case err != nil:
			return nil, fmt.Errorf("p.db.QueryRow(...): %w", err)
		default:
			return nil, model.ErrDuplicateWallet
		}

		query = `
		UPDATE wallets
		SET name = $2, modified_at = $3
		WHERE is_disabled = false and id = $1 
		RETURNING id, name, modified_at`

		err = tx.QueryRow(
			ctx,
			query,
			walletID, request.Name, time.Now(),
		).Scan(
			&wallet.ID,
			&wallet.Name,
			&wallet.ModifiedDate)
		if err != nil {
			return nil, fmt.Errorf("p.db.QueryRow(...): %w", err)
		}
	}

	if request.Currency != nil {
		query := `
		UPDATE wallets
		SET currency = $2, modified_at = $3, balance = $4
		WHERE is_disabled = false and id = $1 
		RETURNING id, currency, balance, modified_at`

		err = tx.QueryRow(
			ctx,
			query,
			walletID, request.Currency, time.Now(), wallet.Balance*request.ConversionRate,
		).Scan(
			&wallet.ID,
			&wallet.Currency,
			&wallet.Balance,
			&wallet.ModifiedDate,
		)
		if err != nil {
			return nil, fmt.Errorf("p.db.QueryRow(...): %w", err)
		}
	}

	if err := tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("tx.Commit(ctx): %w", err)
	}

	return wallet, nil
}

func (p *Postgres) ExternalTransaction(ctx context.Context, transaction model.Transaction) (*uuid.UUID, error) {
	tx, err := p.Db.Begin(ctx)
	if err != nil {
		return nil, fmt.Errorf("p.db.Begin(ctx): %w", err)
	}

	defer func() {
		err := tx.Rollback(ctx)
		if err != nil && !errors.Is(err, pgx.ErrTxClosed) {
			zap.L().With(zap.Error(err)).Warn("ExternalTransaction/tx.Rollback(ctx)")
		}
	}()

	// Save transaction
	query := `
	INSERT INTO transactions (id, to_wallet_id, currency, balance)
	VALUES ($1, $2, $3, $4)
	returning id, created_at`

	err = tx.QueryRow(
		ctx,
		query,
		transaction.ID, transaction.TargetWalletID, transaction.Currency, transaction.Sum,
	).Scan(
		&transaction.ID,
		&transaction.CreatedAt,
	)

	var pgErr *pgconn.PgError

	switch {
	case errors.As(err, &pgErr) && pgErr.Code == pgerrcode.UniqueViolation:
		return nil, model.ErrDuplicateTransaction
	case err != nil:
		return nil, fmt.Errorf("tx.QueryRow(): %w", err)
	}

	// Update wallet
	query = `
	UPDATE wallets
	SET balance = balance + $1, modified_at = $3
	WHERE id = $2
	RETURNING currency`

	var currency string

	err = tx.QueryRow(
		ctx,
		query,
		transaction.Sum, transaction.TargetWalletID, time.Now(),
	).Scan(
		&currency)

	switch {
	case errors.Is(err, pgx.ErrNoRows):
		return nil, model.ErrWalletNotFound
	case errors.As(err, &pgErr) && pgErr.Code == pgerrcode.CheckViolation:
		return nil, model.ErrNotEnoughBalance
	case currency != transaction.Currency:
		return nil, model.ErrWalletWasChanged
	case err != nil:
		return nil, fmt.Errorf("tx.Exec(ctx, query, targetWallet.Sum, targetWallet.ID): %w", err)
	}

	// Commit transaction
	if err := tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("tx.Commit(ctx): %w", err)
	}

	return &transaction.ID, nil
}
