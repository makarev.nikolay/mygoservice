package users

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
	"gitlab.com/makarev.nikolay/mygoservice/internal/store"
)

func Create(ctx context.Context, user model.User, p *store.Postgres) (*model.User, error) {
	query := `
	INSERT INTO users (id, email)
	VALUES ($1, $2)
	RETURNING id, registered_at
`

	err := p.Db.QueryRow(
		ctx,
		query,
		uuid.New(),
		user.Email,
	).Scan(
		&user.ID,
		&user.RegDate,
	)
	if err != nil {
		return nil, fmt.Errorf("p.db.QueryRow(...): %w", err)
	}

	return &user, nil
}
