package apiserver

import (
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
	"go.uber.org/zap"
)

func (s *APIServer) getWalletByID(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeErrorResponse(w, http.StatusBadRequest, "can't get id")

		return
	}

	wallet, err := s.service.GetWalletByID(r.Context(), id)

	switch {
	case errors.Is(err, model.ErrNilUUID):
		fallthrough
	case errors.Is(err, model.ErrNotAllowed):
		fallthrough
	case errors.Is(err, model.ErrWalletNotFound):
		writeErrorResponse(w, http.StatusNotFound, "wallet not found")

		return
	case err != nil:
		zap.L().With(zap.Error(err)).Warn("getWalletsByID/s.service.getWalletsByID(r.Context(), walletID))")
		writeErrorResponse(w, http.StatusInternalServerError, "internal server error")

		return
	}

	writeOkResponse(w, http.StatusOK, wallet)

	zap.L().Debug("successful GET:/wallet/{id}", zap.String("client", r.RemoteAddr))
}
