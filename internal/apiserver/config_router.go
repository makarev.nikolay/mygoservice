package apiserver

import "github.com/go-chi/chi/v5"

func (s *APIServer) configRouter() {
	s.router.Route("/api", func(r chi.Router) {
		r.Use(s.JWTAuth)

		r.Get("/helloWorld", s.getHelloWorld)
		r.Post("/wallets", s.createWallet)
		r.Get("/wallets", s.getWallets)
		r.Get("/wallets/{id}", s.getWalletByID)
		r.Delete("/wallets/{id}", s.deleteWallet)
		r.Patch("/wallets/{id}", s.updateWallet)
		r.Put("/wallets/deposit", s.deposit)
	})
}
