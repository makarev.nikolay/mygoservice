package apiserver

import "net/http"

func (s *APIServer) getHelloWorld(w http.ResponseWriter, _ *http.Request) {
	_, err := w.Write([]byte("Hello, World!"))
	if err != nil {
		return
	}
}
