package apiserver

import (
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
	"go.uber.org/zap"
)

func (s *APIServer) deleteWallet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeErrorResponse(w, http.StatusBadRequest, "can't get id")

		return
	}

	err = s.service.DeleteWallet(r.Context(), id)

	switch {
	case errors.Is(err, model.ErrNilUUID):
		fallthrough
	case errors.Is(err, model.ErrWalletNotFound):
		writeErrorResponse(w, http.StatusNotFound, "wallet not found")

		return
	case err != nil:
		zap.L().With(zap.Error(err)).Warn("deleteWallet/s.service.DeleteWallet(r.Context(), id)")
		writeErrorResponse(w, http.StatusInternalServerError, "internal server error")

		return
	}

	w.WriteHeader(http.StatusNoContent)

	zap.L().Debug("successful DELETE:/wallets/{id}", zap.String("client", r.RemoteAddr))
}
