package apiserver

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
	"go.uber.org/zap"
)

func (s *APIServer) updateWallet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeErrorResponse(w, http.StatusBadRequest, "can't get id")

		return
	}

	var updateRequest model.UpdateWalletRequest

	if err := json.NewDecoder(r.Body).Decode(&updateRequest); err != nil {
		writeErrorResponse(w, http.StatusBadRequest, "failed to read body")

		return
	}

	wallet, err := s.service.UpdateWallet(r.Context(), id, updateRequest)

	switch {
	case errors.Is(err, model.ErrNilUUID):
		fallthrough
	case errors.Is(err, model.ErrWalletNotFound):
		writeErrorResponse(w, http.StatusNotFound, "wallet not found")

		return
	case errors.Is(err, model.ErrDuplicateWallet):
		writeErrorResponse(w, http.StatusUnprocessableEntity, "duplicate names not allowed")

		return
	case errors.Is(err, model.ErrWrongCurrency):
		writeErrorResponse(w, http.StatusUnprocessableEntity, "wrong currency")

		return
	case errors.Is(err, model.ErrNotAllowed):
		writeErrorResponse(w, http.StatusUnauthorized, "operation not allowed")

		return
	case err != nil:
		zap.L().With(zap.Error(err)).Warn(
			"updateWallet/s.service.UpdateWallet(r.Context(), id, updateRequest)")
		writeErrorResponse(w, http.StatusInternalServerError, "internal server error")

		return
	}

	writeOkResponse(w, http.StatusOK, wallet)

	zap.L().Debug("successful PATCH:/wallets/{id}", zap.String("client", r.RemoteAddr))
}
