package apiserver

import (
	"context"
	"crypto/rsa"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/makarev.nikolay/mygoservice/internal/config"
	"go.uber.org/zap"
)

type APIServer struct {
	router  *chi.Mux
	server  *http.Server
	key     *rsa.PublicKey
	service service
}

type Config struct {
	BindAddress string
}

func New(cfg *config.Config, service service, key *rsa.PublicKey) *APIServer {
	router := chi.NewRouter()

	zap.L().Info("server init")

	return &APIServer{
		service: service,
		router:  router,
		key:     key,
		server: &http.Server{
			Addr:              cfg.HTTPServerConf.BindAddress,
			ReadHeaderTimeout: cfg.HTTPServerConf.ServerTimeout,
			Handler:           router,
		},
	}
}

func (s *APIServer) Run(ctx context.Context, cfg *config.Config) error {
	zap.L().Debug("starting api server")
	defer zap.L().Info("server stopped")

	s.configRouter()

	zap.L().Debug("configured router")

	go func() {
		<-ctx.Done()

		zap.L().Debug("closing server")

		gfCtx, cancel := context.WithTimeout(context.Background(), cfg.HTTPServerConf.IdleTimeout)
		defer cancel()

		zap.L().Debug("attempting graceful shutdown")

		//nolint: contextcheck
		if err := s.server.Shutdown(gfCtx); err != nil {
			zap.L().With(zap.Error(err)).Warn("failed to gracefully shutdown server")

			return
		}
	}()

	if err := s.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		zap.L().Info("server error", zap.Error(err))
	}

	return nil
}
