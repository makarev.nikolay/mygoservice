package apiserver

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
)

type HTTPResponse struct {
	Data  any    `json:"data,omitempty"`
	Error string `json:"error,omitempty"`
}

type service interface {
	CreateWallet(ctx context.Context, user model.Wallet) (*model.Wallet, error)
	GetWallets(ctx context.Context) ([]*model.Wallet, error)
	GetWalletByID(ctx context.Context, walletID uuid.UUID) (*model.Wallet, error)
	DeleteWallet(ctx context.Context, walletID uuid.UUID) error
	UpdateWallet(ctx context.Context, walletID uuid.UUID, request model.UpdateWalletRequest) (*model.Wallet, error)
	ExternalTransaction(ctx context.Context, transaction model.Transaction) (*uuid.UUID, error)
}
