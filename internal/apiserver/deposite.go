package apiserver

import (
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
	"go.uber.org/zap"
	"net/http"
)

type TransferResponse struct {
	TransactionID uuid.UUID `json:"transactionId"`
}

func (s *APIServer) deposit(w http.ResponseWriter, r *http.Request) {
	var requestTransaction model.Transaction

	err := json.NewDecoder(r.Body).Decode(&requestTransaction)
	if err != nil {
		writeErrorResponse(w, http.StatusBadRequest, "failed to read body")

		return
	}

	err = requestTransaction.Validate()
	if err != nil {
		writeErrorResponse(w, http.StatusUnprocessableEntity, err.Error())

		return
	}

	transferID, err := s.service.ExternalTransaction(r.Context(), requestTransaction)

	switch {
	case errors.Is(err, model.ErrWalletNotFound):
		writeErrorResponse(w, http.StatusNotFound, "wallet not found")

		return
	case errors.Is(err, model.ErrWrongCurrency):
		fallthrough
	case errors.Is(err, model.ErrNilUUID):
		fallthrough
	case errors.Is(err, model.ErrNegativeSum):
		writeErrorResponse(w, http.StatusUnprocessableEntity, "incorrect request data")

		return
	case errors.Is(err, model.ErrDuplicateTransaction):
		writeErrorResponse(w, http.StatusTooManyRequests, "transaction already exists")

		return
	case err != nil:
		zap.L().With(zap.Error(err)).Warn("deposit/s.service.ExternalTransaction(r.Context(), requestTransaction)")
		writeErrorResponse(w, http.StatusInternalServerError, "internal server error")

		return
	}

	writeOkResponse(w, http.StatusOK, TransferResponse{TransactionID: *transferID})

	zap.L().Debug("successful PUT:/deposit", zap.String("client", r.RemoteAddr))
}
