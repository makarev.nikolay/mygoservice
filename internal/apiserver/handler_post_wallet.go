package apiserver

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
	"go.uber.org/zap"
)

func (s *APIServer) createWallet(w http.ResponseWriter, r *http.Request) {
	var rWallet model.Wallet

	if err := json.NewDecoder(r.Body).Decode(&rWallet); err != nil {
		writeErrorResponse(w, http.StatusBadRequest, "failed to read body")

		return
	}

	//rWallet.OwnerID = userInfo.ID

	wallet, err := s.service.CreateWallet(r.Context(), rWallet)

	switch {
	case errors.Is(err, model.ErrNilUUID):
		fallthrough
	case errors.Is(err, model.ErrDuplicateWallet):
		writeErrorResponse(w, http.StatusUnprocessableEntity, "wallet name is already taken")

		return
	case err != nil:
		zap.L().With(zap.Error(err)).Warn(
			"createWallet/s.service.CreateWallet(r.Context(), model.User{ID: rWallet.OwnerID}, rWallet.Currency)")
		writeErrorResponse(w, http.StatusInternalServerError, "internal server error")

		return
	}

	writeOkResponse(w, http.StatusCreated, wallet)

	zap.L().Debug("successful POST:/wallet", zap.String("client", r.RemoteAddr))
}
