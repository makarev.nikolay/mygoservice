package apiserver

import (
	"net/http"

	"go.uber.org/zap"
)

func (s *APIServer) getWallets(w http.ResponseWriter, r *http.Request) {
	wallets, err := s.service.GetWallets(r.Context())
	if err != nil {
		zap.L().With(zap.Error(err)).Warn("getWallets/s.service.getWallets(r.Context(), rWallet)")
		writeErrorResponse(w, http.StatusInternalServerError, "internal server error")

		return
	}

	writeOkResponse(w, http.StatusOK, wallets)

	zap.L().Debug("successful GET:/wallets", zap.String("client", r.RemoteAddr))
}
