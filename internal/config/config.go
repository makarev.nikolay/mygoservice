package config

import (
	"os"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
	"go.uber.org/zap"
)

type Config struct {
	XRBindAddr     string         `yaml:"xrBindAddr"`
	HTTPServerConf HTTPServerConf `yaml:"httpServer"`
	LogConf        LogConf        `yaml:"logConf"`
	PGConf         PGConf         `yaml:"pgConf"`
}

type HTTPServerConf struct {
	BindAddress   string        `yaml:"bindAddress"`
	ServerTimeout time.Duration `yaml:"serverTimeout"`
	IdleTimeout   time.Duration `yaml:"idleTimeout"`
}

type LogConf struct {
	Level string `yaml:"logLevel"`
}

type PGConf struct {
	PGHost     string `yaml:"pgHost"`
	PGPort     string `yaml:"pgPort"`
	PGDatabase string `yaml:"pgDatabase"`
	PGUser     string `yaml:"pgUser"`
	PGPassword string `yaml:"pgPassword"`
}

func New() (*Config, error) {
	//configPath := "../config/config.yaml"
	configPath := "./config/config.yaml"
	if configPath == "" {
		zap.L().With().Panic("CONFIG_PATH is not set")
	}

	_, err := os.Stat(configPath)

	if os.IsNotExist(err) {
		zap.L().With(zap.Error(err)).Panic("config file %s does not exist")
	}

	cfg := Config{}

	err = cleanenv.ReadConfig(configPath, &cfg)
	if err != nil {
		return nil, err
	}

	err = cleanenv.ReadEnv(&cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, nil
}
