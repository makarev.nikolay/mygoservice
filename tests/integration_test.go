package tests

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/google/uuid"
	migrate "github.com/rubenv/sql-migrate"
	"gitlab.com/makarev.nikolay/mygoservice/internal/apiserver"
	"gitlab.com/makarev.nikolay/mygoservice/internal/config"
	"gitlab.com/makarev.nikolay/mygoservice/internal/currconv"
	"gitlab.com/makarev.nikolay/mygoservice/internal/logger"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
	_ "gitlab.com/makarev.nikolay/mygoservice/internal/model"
	"gitlab.com/makarev.nikolay/mygoservice/internal/service"
	"gitlab.com/makarev.nikolay/mygoservice/internal/store"
	"gitlab.com/makarev.nikolay/mygoservice/internal/store/users"
	_ "gitlab.com/makarev.nikolay/mygoservice/internal/store/users"
	"net/http"
	"os/signal"
	"syscall"
	"testing"

	"github.com/stretchr/testify/suite"
)

const (
	walletEndpoint   = "/wallets"
	bindAddr         = "http://localhost:8080/api"
	currencyEUR      = "EUR"
	currencyUSD      = "USD"
	standardName     = "good wallet"
	secondaryName    = "better wallet"
	thirdName        = "best wallet"
	fourthName       = "fourth name wallet"
	badRequestString = "Lorem Ipsum?"
)

type currencyConverter interface {
	GetExchangeRate(baseCurrency, targetCurrency string) (float64, error)
}

type IntegrationTestSuite struct {
	suite.Suite
	ctx *context.Context

	testOwnerID   uuid.UUID
	secondOwnerID uuid.UUID

	str       *store.Postgres
	converter currencyConverter
}

func TestIntegrationTestSuite(t *testing.T) {
	suite.Run(t, new(IntegrationTestSuite))
}

func (s *IntegrationTestSuite) SetupSuite() {
	s.testOwnerID = uuid.New()

	ctx, _ := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	s.ctx = &ctx

	cfg, err := config.New()

	logger.InitLogger(cfg)

	str, err := store.New(ctx, cfg)
	s.Require().NoError(err)

	err = str.Migrate(migrate.Up)
	s.Require().NoError(err)

	user, err := users.Create(ctx, model.User{Email: "test@test.com"}, str)
	s.Require().NoError(err)
	s.testOwnerID = user.ID
	s.Require().NoError(err)

	user, err = users.Create(ctx, model.User{Email: "test2@test.com"}, str)
	s.Require().NoError(err)
	s.secondOwnerID = user.ID
	s.Require().NoError(err)
	s.str = str
	s.converter = currconv.New(cfg.XRBindAddr)

	srv := service.New(str, s.converter)

	server := apiserver.New(cfg, srv)

	go func() {
		err = server.Run(ctx, cfg)
		s.Require().NoError(err)
	}()
}

func (s *IntegrationTestSuite) TearDownSuite() {
	err := s.str.TruncateTables(context.Background())
	s.Require().NoError(err)
}

func (s *IntegrationTestSuite) TestWallets() {
	wallet1 := model.Wallet{
		OwnerID:  s.testOwnerID,
		Currency: currencyEUR,
		Name:     standardName,
		Balance:  0,
	}

	wallet2 := model.Wallet{
		OwnerID:  s.testOwnerID,
		Currency: currencyEUR,
		Name:     secondaryName,
		Balance:  0,
	}

	wallet3 := model.Wallet{
		OwnerID:  s.testOwnerID,
		Currency: currencyUSD,
		Name:     thirdName,
		Balance:  0,
	}

	s.Run("wallets", func() {
		s.Run("POST:/wallets", func() {
			s.Run("201", func() {
				s.checkWalletPost(&wallet1)
				s.checkWalletPost(&wallet2)
				s.checkWalletPost(&wallet3)
			})

			s.Run("422/duplicate name", func() {
				var respWalletData model.Wallet

				resp := s.sendRequest(
					context.Background(),
					http.MethodPost,
					walletEndpoint,
					wallet1,
					&apiserver.HTTPResponse{Data: &respWalletData})
				s.Require().Equal(http.StatusUnprocessableEntity, resp.StatusCode)
			})

			s.Run("400", func() {
				resp := s.sendRequest(
					context.Background(),
					http.MethodPost,
					walletEndpoint,
					badRequestString,
					nil)
				s.Require().Equal(http.StatusBadRequest, resp.StatusCode)
			})
		})

		s.Run("GET:/wallets", func() {
			s.Run("200", func() {
				var wallets []model.Wallet

				params := "?limit=10&sorting=created_at&descending=true"

				resp := s.sendRequest(
					context.Background(),
					http.MethodGet,
					walletEndpoint+params,
					nil,
					&apiserver.HTTPResponse{Data: &wallets})

				walletsFound := 0

				for _, value := range wallets {
					if value.ID == wallet1.ID || value.ID == wallet2.ID || value.ID == wallet3.ID {
						walletsFound++
					}
				}

				s.Require().Equal(http.StatusOK, resp.StatusCode)
				s.Require().NotZero(len(wallets))
				s.Require().Equal(3, walletsFound)
			})
		})
	})
}

func (s *IntegrationTestSuite) sendRequest(ctx context.Context, method, endpoint string, body interface{}, dest interface{}) *http.Response {
	s.T().Helper()

	reqBody, err := json.Marshal(body)
	s.Require().NoError(err)

	req, err := http.NewRequestWithContext(ctx, method, bindAddr+endpoint, bytes.NewReader(reqBody))
	s.Require().NoError(err)

	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	s.Require().NoError(err)

	defer func() {
		err = resp.Body.Close()
		s.Require().NoError(err)
	}()

	if dest != nil {
		err = json.NewDecoder(resp.Body).Decode(&dest)
		s.Require().NoError(err)
	}

	return resp
}

func (s *IntegrationTestSuite) checkWalletPost(wallet *model.Wallet) {
	var respWalletData model.Wallet

	resp := s.sendRequest(context.Background(), http.MethodPost, walletEndpoint, wallet, &apiserver.HTTPResponse{Data: &respWalletData})
	s.Require().Equal(http.StatusCreated, resp.StatusCode)
	s.Require().Equal(wallet.Currency, respWalletData.Currency)
	s.Require().Equal(wallet.OwnerID, respWalletData.OwnerID)
	s.Require().Equal(wallet.Balance, respWalletData.Balance)
	s.Require().Equal(wallet.Name, respWalletData.Name)
	s.Require().NotZero(respWalletData.ID)
	wallet.ID = respWalletData.ID
}
