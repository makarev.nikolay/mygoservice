package main

import (
	"context"
	"encoding/json"
	"github.com/google/uuid"
	"gitlab.com/makarev.nikolay/mygoservice/internal/jwtgenerator"
	"gitlab.com/makarev.nikolay/mygoservice/internal/model"
	"os/signal"
	"syscall"

	migrate "github.com/rubenv/sql-migrate"
	"gitlab.com/makarev.nikolay/mygoservice/internal/apiserver"
	"gitlab.com/makarev.nikolay/mygoservice/internal/config"
	"gitlab.com/makarev.nikolay/mygoservice/internal/currconv"
	"gitlab.com/makarev.nikolay/mygoservice/internal/logger"
	"gitlab.com/makarev.nikolay/mygoservice/internal/service"
	"gitlab.com/makarev.nikolay/mygoservice/internal/store"
	"go.uber.org/zap"
)

func main() {

	a := make([]int, 5)
	a = append(a, 1)
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	defer cancel()

	cfg, err := config.New()
	if err != nil {
		panic(err)
	}
	err = logger.InitLogger(cfg)
	if err != nil {
		panic(err)
	}

	pgStore, err := store.New(ctx, cfg.PGConf)
	if err != nil {
		zap.L().With(zap.Error(err)).Panic("pgStore.New")
	}

	if err := pgStore.Migrate(migrate.Up); err != nil {
		zap.L().With(zap.Error(err)).Panic("pgStore.Migrate")
	}
	converter := currconv.New(cfg.XRBindAddr)
	serviceLayer := service.New(pgStore, converter)
	jwtGenerator := jwtgenerator.NewJWTGenerator()
	myUuid, err := uuid.Parse("eafeb388-0fb2-4c89-aa76-d86c590343c5")
	user := model.User{ID: myUuid, Email: "test@test.test"}
	output, err := jwtGenerator.GetNewTokenString(user)
	logData, err := json.Marshal(map[string]string{"jwt": output})

	zap.L().Info(string(logData))

	server := apiserver.New(
		cfg,
		serviceLayer,
		jwtGenerator.GetPublicKey(),
	)

	err = server.Run(ctx, cfg)
	if err != nil {
		zap.L().Info("server error", zap.Error(err))

		return
	}
}
