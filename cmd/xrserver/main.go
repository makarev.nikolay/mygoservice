package main

import (
	"context"
	"os/signal"
	"syscall"

	"gitlab.com/makarev.nikolay/mygoservice/internal/config"
	"gitlab.com/makarev.nikolay/mygoservice/internal/logger"
	server "gitlab.com/makarev.nikolay/mygoservice/internal/xrserver"
	"go.uber.org/zap"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	defer cancel()

	cfg, err := config.New()
	if err != nil {
		panic(err)
	}
	err = logger.InitLogger(cfg)
	if err != nil {
		panic(err)
	}

	// no error handling for now
	// check https://github.com/uber-go/zap/issues/991
	//nolint: errcheck
	defer zap.L().Sync()

	s := server.New(cfg.XRBindAddr)

	if err := s.Run(ctx); err != nil {
		zap.L().With(zap.Error(err)).Panic("error running server")
	}
}
