BINARY_NAME = main
SRC_DIR = .

xr:
	CGO_ENABLED=0 GOOS=linux go build -o bin/xrserver cmd/xrserver/main.go
	./bin/xrserver/$(BINARY_NAME)


build:
	CGO_ENABLED=0 GOOS=linux go build -o bin/apiserver cmd/apiserver/main.go

run: build
	./bin/apiserver/$(BINARY_NAME)

test:
	go test ./...

clean:
	rm -f $(BINARY_NAME)

fmt:
	go fmt ./...

mod:
	go mod tidy

vendor:
	go mod vendor

lint:
	golangci-lint run

docker:
	docker-compose up
pg-up:
	docker-compose up -d pg
pg-down:
	docker-compose stop pg
# Специальные цели
.PHONY: build run test clean fmt mod vendor lint docker pg-up pg-down
